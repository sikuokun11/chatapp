/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trinhtantai.ChatDatingApp.controller;

import com.trinhtantai.ChatDatingApp.model.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author trinhtantai
 */
@RestController
public class ChatController {
    
    @MessageMapping("chat.register")
    @SendTo("/topic/public")
    public Message register(@Payload Message chatMessage,SimpMessageHeaderAccessor headerAccessor){
        headerAccessor.getSessionAttributes().put("username",chatMessage.getSender());
        return chatMessage;
    }
    @MessageMapping("chat.send")
    @SendTo("/topic/public")
    public Message sendMessage(@Payload Message chatMessage){
    return  chatMessage;
    }
    @GetMapping("/chat")
    public String chatchat(){
        return "HELLO BOYS";
    }
}
