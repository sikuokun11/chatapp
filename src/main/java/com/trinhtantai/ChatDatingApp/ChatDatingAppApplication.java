package com.trinhtantai.ChatDatingApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatDatingAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChatDatingAppApplication.class, args);
	}

}
